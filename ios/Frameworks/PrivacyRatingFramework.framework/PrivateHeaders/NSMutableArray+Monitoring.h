//
//  NSMutableArray+Monitoring.h
//  PrivacyRatingFramework
//
//  Created by Ari Deane on 25/11/2018.
//  Copyright © 2018 PrivacyRating. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSMutableArray (Monitoring)

@end

NS_ASSUME_NONNULL_END
